/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mydcoffeeproject.service;

import com.mycompany.mydcoffeeproject.dao.SalaryDao;
import com.mycompany.mydcoffeeproject.model.Salary;
import java.util.List;

/**
 *
 * @author Admin
 */
public class SalaryService {
    public Salary getById(int id) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.get(id);
    }
    
    public List<Salary> getSalary(){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll(" emp_id asc");
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.delete(editedSalary);
    }


}
