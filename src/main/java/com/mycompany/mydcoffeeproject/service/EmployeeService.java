/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mydcoffeeproject.service;


import com.mycompany.mydcoffeeproject.dao.EmployeeDao;
import com.mycompany.mydcoffeeproject.model.Employee;
import java.util.List;

/**
 *
 * @author werapan
 */
public class EmployeeService {
    public Employee getById(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.get(id);
    }
    
    public List<Employee> getEmployee(){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" emp_id asc");
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }

}
