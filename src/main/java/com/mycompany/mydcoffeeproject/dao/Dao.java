/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.mydcoffeeproject.dao;


import java.util.List;

/**
 *
 * @author Jedsada
 */
public interface Dao<E> {
    E get(int id);
    List<E> getAll();
    E save(E obj);
    E update(E obj);
    int delete(E obj);
}
