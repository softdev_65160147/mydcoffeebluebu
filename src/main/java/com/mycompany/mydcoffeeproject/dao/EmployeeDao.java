/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mydcoffeeproject.dao;


import com.mycompany.mydcoffeeproject.helper.DatabaseHelper;
import com.mycompany.mydcoffeeproject.model.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class EmployeeDao implements Dao<Employee> {

    @Override
    public Employee get(int id) {
        Employee Employee = null;
        String sql = "SELECT * FROM EMPLOYEE WHERE emp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Employee = Employee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return Employee;
    }

//    public Employee getByLogin(String name) {
//        Employee Employee = null;
//        String sql = "SELECT * FROM Employee WHERE emp_f_name=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, name);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                Employee = Employee.fromRS(rs);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return Employee;
//    }
    @Override
    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Employee> getAll(String where, String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Employee> getAll(String order) {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Employee save(Employee obj) {

        String sql = "INSERT INTO EMPLOYEE (emp_id, emp_f_name, emp_l_name, emp_username, emp_password, emp_pos, emp_tel , emp_email)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            stmt.setString(2, obj.getFname());
            stmt.setString(3, obj.getLname());
            stmt.setString(4, obj.getUsername());
            stmt.setString(5, obj.getPassword());
            stmt.setInt(6, obj.getPos());
            stmt.setString(7, obj.getTel());
            stmt.setString(8, obj.getEmail());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE EMPLOYEE"
                + " SET emp_f_name = ?, emp_l_name = ?, emp_username = ?, emp_password = ?, emp_pos = ?, emp_tel = ?, emp_email = ?"
                + " WHERE emp_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getFname());
            stmt.setString(3, obj.getLname());
            stmt.setString(4,obj.getUsername());
            stmt.setString(5, obj.getPassword());
            stmt.setInt(6, obj.getPos());
            stmt.setString(7, obj.getTel());
            stmt.setString(8, obj.getEmail());
            
           
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM EMPLOYEE WHERE emp_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }


}
