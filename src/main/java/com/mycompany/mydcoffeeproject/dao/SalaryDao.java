/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mydcoffeeproject.dao;

import com.mycompany.mydcoffeeproject.helper.DatabaseHelper;
import com.mycompany.mydcoffeeproject.model.Salary;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class SalaryDao implements Dao<Salary>{
    
    
    @Override
    public Salary get(int id) {
        Salary Salary = null;
        String sql = "SELECT * FROM SALARY WHERE sl_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Salary = Salary.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return Salary;
    }

//    public Salary getByLogin(String name) {
//        Salary Salary = null;
//        String sql = "SELECT * FROM Salary WHERE emp_fname=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, name);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                Salary = Salary.fromRS(rs);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return Salary;
//    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM SALARY";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Salary> getAll(String where, String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM SALARY where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Salary> getAll(String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM SALARY  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Salary save(Salary obj) {
        String sql = "INSERT INTO SALARY (sl_id, emp_id, sl_type, sl_total_hr, sl_rate_hr, sl_net_income)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setInt(2, obj.getEmpId());
            stmt.setInt(3, obj.getType());
            stmt.setInt(4, obj.getTotalHr());
            stmt.setInt(5, obj.getRateHr());
            stmt.setInt(6, obj.getNetIncome());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(1);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Salary update(Salary obj) {
        String sql = "UPDATE SALARY"
                + " SET emp_id = ?, sl_type = ?, sl_total_hr = ?, sl_rate_hr = ?, sl_net_income = ?"
                + " WHERE sl_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1, obj.getEmpId());
            stmt.setInt(2, obj.getType());
            stmt.setInt(3, obj.getTotalHr());
            stmt.setInt(4, obj.getRateHr());
            stmt.setInt(5, obj.getNetIncome());
            
            stmt.setInt(6, obj.getId()); 
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Salary obj) {
        String sql = "DELETE FROM SALARY WHERE sl_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}
