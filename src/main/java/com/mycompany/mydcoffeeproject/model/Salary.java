/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mydcoffeeproject.model;

import com.mycompany.mydcoffeeproject.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Salary {
    private int id;
    private int empId;
    private int type;
    private int totalHr;
    private int rateHr;
    private int netIncome;

    public Salary(int id, int empId, int type, int totalHr, int rateHr, int netIncome) {
        this.id = id;
        this.empId = empId;
        this.type = type;
        this.totalHr = totalHr;
        this.rateHr = rateHr;
        this.netIncome = netIncome;
    }
    
    public Salary(int empId, int type, int totalHr, int rateHr, int netIncome) {
        this.id = -1;
        this.empId = empId;
        this.type = type;
        this.totalHr = totalHr;
        this.rateHr = rateHr;
        this.netIncome = netIncome;
    }
    
    public Salary() {
        this.id = -1;
        this.empId = 0;
        this.type = 0;
        this.totalHr = 0;
        this.rateHr = 0;
        this.netIncome = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getTotalHr() {
        return totalHr;
    }

    public void setTotalHr(int totalHr) {
        this.totalHr = totalHr;
    }

    public int getRateHr() {
        return rateHr;
    }

    public void setRateHr(int rateHr) {
        this.rateHr = rateHr;
    }

    public int getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(int netIncome) {
        this.netIncome = netIncome;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", empId=" + empId + ", type=" + type + ", totalHr=" + totalHr + ", rateHr=" + rateHr + ", netIncome=" + netIncome + '}';
    }
    

    public static Salary fromRS(ResultSet rs) {
    Salary salary = new Salary();
    try {
        salary.setId(rs.getInt("sl_id"));
        salary.setEmpId(rs.getInt("emp_id"));
        salary.setType(rs.getInt("sl_type"));
        salary.setTotalHr(rs.getInt("sl_total_hr"));
        salary.setRateHr(rs.getInt("sl_rate_hr"));
        salary.setNetIncome(rs.getInt("sl_net_income"));
    } catch (SQLException ex) {
        Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
        return null;
    }
    return salary;
}

    
}

