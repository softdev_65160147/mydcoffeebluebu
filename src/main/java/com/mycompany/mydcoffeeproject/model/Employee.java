/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mydcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Employee {

    private int id;
    private String password;
    private String username;
    private String fname;
    private String lname;
    private String tel;
    private String email;
    private int pos;

    public Employee(int id, String password, String username, String fname, String lname, String tel, String email, int pos) {
        this.id = id;
        this.password = password;
        this.username = username;
        this.fname = fname;
        this.lname = lname;
        this.tel = tel;
        this.email = email;
        this.pos = pos;
    }

    public Employee(String password, String username, String fname, String lname, String tel, String email, int pos) {
        this.id = -1;
        this.password = password;
        this.username = username;
        this.fname = fname;
        this.lname = lname;
        this.tel = tel;
        this.email = email;
        this.pos = pos;
    }
     public Employee() {
        this.id = -1;
        this.password = "";
        this.username = "";
        this.fname = "";
        this.lname = "";
        this.tel = "";
        this.email = "";
        this.pos = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", password=" + password + ", username=" + username + ", fname=" + fname + ", lname=" + lname + ", tel=" + tel + ", email=" + email + ", pos=" + pos + '}';
    }


   

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("emp_id"));
            employee.setFname(rs.getString("emp_f_name"));
            employee.setLname(rs.getString("emp_l_name"));
            employee.setUsername(rs.getString("emp_username"));
            employee.setPassword(rs.getString("emp_password"));
            employee.setTel(rs.getString("emp_tel"));
            employee.setEmail(rs.getString("emp_email"));
            employee.setPos(rs.getInt("emp_pos"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
}
